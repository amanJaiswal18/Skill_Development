#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<regex.h>
#define BUF_SIZE 345615

int main(int argc, char **argv){
  FILE *fp1,*fp2;
  char buffer[BUF_SIZE];
  char msgbuf[100];
  int length_string,i=0,reti;
  regex_t regex;
  fp1 = fopen(argv[1],"r");
  if( fp1 == NULL ){
    perror("Error while opening the file.\n");
    exit(EXIT_FAILURE);
  }
  fread(buffer,1,BUF_SIZE,fp1);
  length_string = strlen(buffer);
  fp2 = fopen("output.txt","w");
  /* Compile regular expression */
  reti = regcomp(&regex,".\\\".+\"",0);
  if(reti){
    fprintf(stderr,"Could not compile regex\n");
    exit(1);
  }
  /* Execute regular expression */
  reti = regexec(&regex, ".\"this is man page for Armstrong thread code.", 0, NULL, 0);
  if(!reti){
    puts("match");
  }
  else if(reti ==REG_NOMATCH){
    puts("not match");
  }
  else{
    regerror(reti, &regex, msgbuf, sizeof(msgbuf));
    fprintf(stderr, "Regex match failed: %s\n", msgbuf);
    exit(1);
  }
  /* Free memory allocated to the pattern buffer by regcomp() */
  regfree(&regex);
  /* while(buffer[i]!='\0'){ */
  /*   printf("%c",buffer[i]); */
  /*   i++; */
  /* } */
}
