#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
//ps -elf | grep a.out to check how many process is created
void* strongNumber (void *arg) {
  int *y = (int *) arg;
  int x =  *y;
  int t = x;
  int s = 0;
  int r;
  // fprintf(stdout, "%d\n", x);
  while(x>0) {
    r = x % 10;
    s += r*r*r;
    x /= 10;
  }
  if(s==t)
    fprintf(stdout, "%d\n", t);
  //  fprintf(stdout, "End.\n");
  sleep(30000);
  pthread_exit(0);
}


int main(int argc, char **argv) {
  pthread_t tid[154];
  if(argc != 2)  {
    fprintf(stderr, "Usage: %s <maximumRange>\n", argv[0]); 
    return -1;
  }
  int n = atoi(argv[1]);
  int i;
  int x = 0;
  for(i=1;i<n;++i) {
    pthread_create(&tid[i], NULL, strongNumber, (void *)&i);
  }
  //  pthread_join(tid, NULL);
  //for(i=1; i<n;++i) 
   // pthread_join(tid[i], NULL); 
  //  fprintf(stdout, "Out of thread.\n");
sleep(30000);
  pthread_exit(0);
 return 0;
}
